<?php

/**
 * Implementation of hook_strongarm().
 */
function muni_events_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_kt_event_calandar';
  $strongarm->value = 0;
  $export['comment_anonymous_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_kt_event_calandar';
  $strongarm->value = '3';
  $export['comment_controls_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_kt_event_calandar';
  $strongarm->value = '4';
  $export['comment_default_mode_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_kt_event_calandar';
  $strongarm->value = '1';
  $export['comment_default_order_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_kt_event_calandar';
  $strongarm->value = '50';
  $export['comment_default_per_page_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_kt_event_calandar';
  $strongarm->value = '0';
  $export['comment_form_location_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_kt_event_calandar';
  $strongarm->value = '2';
  $export['comment_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_kt_event_calandar';
  $strongarm->value = '1';
  $export['comment_preview_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_kt_event_calandar';
  $strongarm->value = '1';
  $export['comment_subject_field_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_kt_event_calandar';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-1',
    'revision_information' => '3',
    'author' => '1',
    'options' => '4',
    'comment_settings' => '5',
    'language' => '0',
    'translation' => '30',
    'menu' => '-2',
    'path' => '8',
    'attachments' => '6',
  );
  $export['content_extra_weights_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:4:field_muni_events_date_fromto';
  $strongarm->value = 'both';
  $export['date:kt_event_calandar:4:field_muni_events_date_fromto'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:4:field_muni_events_date_multiple_from';
  $strongarm->value = '';
  $export['date:kt_event_calandar:4:field_muni_events_date_multiple_from'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:4:field_muni_events_date_multiple_number';
  $strongarm->value = '';
  $export['date:kt_event_calandar:4:field_muni_events_date_multiple_number'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:4:field_muni_events_date_multiple_to';
  $strongarm->value = '';
  $export['date:kt_event_calandar:4:field_muni_events_date_multiple_to'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:4:field_muni_events_date_show_repeat_rule';
  $strongarm->value = 'show';
  $export['date:kt_event_calandar:4:field_muni_events_date_show_repeat_rule'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:full:field_muni_events_date_fromto';
  $strongarm->value = 'both';
  $export['date:kt_event_calandar:full:field_muni_events_date_fromto'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:full:field_muni_events_date_multiple_from';
  $strongarm->value = '';
  $export['date:kt_event_calandar:full:field_muni_events_date_multiple_from'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:full:field_muni_events_date_multiple_number';
  $strongarm->value = '';
  $export['date:kt_event_calandar:full:field_muni_events_date_multiple_number'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:full:field_muni_events_date_multiple_to';
  $strongarm->value = '';
  $export['date:kt_event_calandar:full:field_muni_events_date_multiple_to'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:full:field_muni_events_date_show_repeat_rule';
  $strongarm->value = 'show';
  $export['date:kt_event_calandar:full:field_muni_events_date_show_repeat_rule'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:teaser:field_muni_events_date_fromto';
  $strongarm->value = 'both';
  $export['date:kt_event_calandar:teaser:field_muni_events_date_fromto'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:teaser:field_muni_events_date_multiple_from';
  $strongarm->value = '';
  $export['date:kt_event_calandar:teaser:field_muni_events_date_multiple_from'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:teaser:field_muni_events_date_multiple_number';
  $strongarm->value = '';
  $export['date:kt_event_calandar:teaser:field_muni_events_date_multiple_number'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:teaser:field_muni_events_date_multiple_to';
  $strongarm->value = '';
  $export['date:kt_event_calandar:teaser:field_muni_events_date_multiple_to'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:kt_event_calandar:teaser:field_muni_events_date_show_repeat_rule';
  $strongarm->value = 'show';
  $export['date:kt_event_calandar:teaser:field_muni_events_date_show_repeat_rule'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_kt_event_calandar';
  $strongarm->value = 1;
  $export['enable_revisions_page_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_kt_event_calandar_field_kt_img_gal';
  $strongarm->value = 0;
  $export['ffp_kt_event_calandar_field_kt_img_gal'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_kt_event_calandar_upload';
  $strongarm->value = 0;
  $export['ffp_kt_event_calandar_upload'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_upload_kt_event_calandar';
  $strongarm->value = array(
    0 => 'file_path_cleanup',
    1 => 'file_name',
    2 => 'file_name_cleanup',
  );
  $export['ffp_upload_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'form_build_id_kt_event_calandar';
  $strongarm->value = 'form-a5de4aaff1961cc60951696b18f413fd';
  $export['form_build_id_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_lock_node_kt_event_calandar';
  $strongarm->value = 0;
  $export['i18n_lock_node_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_newnode_current_kt_event_calandar';
  $strongarm->value = 0;
  $export['i18n_newnode_current_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_kt_event_calandar';
  $strongarm->value = '1';
  $export['i18n_node_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_required_node_kt_event_calandar';
  $strongarm->value = 1;
  $export['i18n_required_node_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18nsync_nodeapi_kt_event_calandar';
  $strongarm->value = array();
  $export['i18nsync_nodeapi_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_kt_event_calandar';
  $strongarm->value = '2';
  $export['language_content_type_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_kt_event_calandar';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_kt_event_calandar__pattern';
  $strongarm->value = '';
  $export['pathauto_node_kt_event_calandar__pattern'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_kt_event_calandar_fr_pattern';
  $strongarm->value = 'evenement/[title-raw]';
  $export['pathauto_node_kt_event_calandar_fr_pattern'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_kt_event_calandar_pattern';
  $strongarm->value = '[type]/[title-raw]';
  $export['pathauto_node_kt_event_calandar_pattern'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_kt_event_calandar';
  $strongarm->value = 0;
  $export['show_diff_inline_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_kt_event_calandar';
  $strongarm->value = 1;
  $export['show_preview_changes_kt_event_calandar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_kt_event_calandar';
  $strongarm->value = '1';
  $export['upload_kt_event_calandar'] = $strongarm;

  return $export;
}
