<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function muni_events_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:passed-events
  $menu_links['primary-links:passed-events'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'passed-events',
    'router_path' => 'passed-events',
    'link_title' => 'Passed events',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '3',
  );
  // Exported menu link: primary-links:upcoming-events
  $menu_links['primary-links:upcoming-events'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'upcoming-events',
    'router_path' => 'upcoming-events',
    'link_title' => 'Events',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('Passed events');


  return $menu_links;
}
