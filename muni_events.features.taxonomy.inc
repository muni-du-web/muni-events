<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function muni_events_taxonomy_default_vocabularies() {
  return array(
    'viala_services' => array(
      'name' => 'Type d\'événement',
      'description' => 'Les services associés à un événement ou à un article',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_viala_services',
      'weight' => '-8',
      'nodes' => array(
        'kt_event_calandar' => 'kt_event_calandar',
      ),
    ),
  );
}
